# Nginx WSGI Site

The Ansible Nginx WSGI Site role will install and enable an nginx site to proxy all requests to a WSGI server except those for serving static content.

Required variables:

- name: Name of the project
- namespace: Name of the client/company
- domains: List of domains the site will serve (eg. www.example.com)

Optional variables:

- prefix_dir: The prefix of the application installation (default: /opt)
- use_ssl: Listen on port 443 for SSL requests (default: no)
- ssl_cert_key: SSL private key used when SSL is turned on (default: "")
- ssl_cert_crt: SSL certificate used when SSL is turned on (default: "")
- wsgi_address: IP address/host of WSGI server
- wsgi_port: Port of WSGI server